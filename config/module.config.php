<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Usuarios;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            #Controller\BlogController::class => InvokableFactory::class
        ]
    ],
    'router' => [
        'routes' => [
            'usuarios' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/usuarios[:/]',
                    'defaults' => [
                        'controller' => Controller\UsuariosController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'usuarios' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/usuarios[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\UsuariosController::class,
                        'action'     => 'index',
                    ],
                ]
            ],
            'roles' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/usuarios/funcoes[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\RolesController::class,
                        'action'     => 'index',
                    ],
                ]
            ],
            'resources' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/usuarios/permissoes[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ResourcesController::class,
                        'action'     => 'index',
                    ],
                ]
            ],
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_map' => [
            'usuarios/index/index' => __DIR__ . '/../view/usuarios/index/index.phtml',

        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

];
