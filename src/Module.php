<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Usuarios;

use Usuarios\Controller\Factory\ResourcesControllerFactory;
use Usuarios\Controller\Factory\RolesControllerFactory;
use Usuarios\Controller\Factory\UsuariosControllerFactory;
use Usuarios\Controller\ResourcesController;
use Usuarios\Controller\RolesController;
use Usuarios\Controller\UsuariosController;
use Usuarios\Form\Factory\ResourceFormFactory;
use Usuarios\Form\Factory\RoleFormFactory;
use Usuarios\Form\Factory\UsuarioEditFormFactory;
use Usuarios\Form\Factory\UsuarioFormFactory;
use Usuarios\Form\ResourceForm;
use Usuarios\Form\RoleForm;
use Usuarios\Form\UsuarioEditForm;
use Usuarios\Form\UsuarioForm;
use Usuarios\Model\Factory\ResourceTableFactory;
use Usuarios\Model\Factory\RoleTableFactory;
use Usuarios\Model\Factory\UsuarioTableFactory;
use Usuarios\Model\Factory\UsuarioTableGatewayFactory;
use Usuarios\Model\Mapper\ResourceTable;
use Usuarios\Model\Mapper\RoleTable;
use Usuarios\Model\Mapper\UsuarioTable;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;



class Module implements ConfigProviderInterface, ServiceProviderInterface, ControllerProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                UsuarioTable::class => UsuarioTableFactory::class,
                RoleTable::class => RoleTableFactory::class,
                UsuarioForm::class => UsuarioFormFactory::class,
                UsuarioEditForm::class => UsuarioEditFormFactory::class,
                RoleForm::class => RoleFormFactory::class,
                ResourceTable::class => ResourceTableFactory::class,
                ResourceForm::class => ResourceFormFactory::class,
            ]
        ];
    }
    public function getControllerConfig()
    {
        return [
            'factories' => [
                UsuariosController::class  =>UsuariosControllerFactory::class,
                RolesController::class     =>RolesControllerFactory::class,
                ResourcesController::class =>ResourcesControllerFactory::class
            ]
        ];
    }

}
