<?php

/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 16:17
 */

namespace Usuarios\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class ResourceForm extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $id = new Element\Hidden("id");
        $this->add($id);

        $resource = new Element\Text("resource");
        $resource->setLabel("Permissão");
        $resource->setAttributes([
                    'class' => 'form-control',
                    'required'=>"required",
                    'data-validation' => "length alphanumeric",
                    'data-validation-length' => "3-12",
                    'data-validation-error-msg' => "Campo requerido - 3 a 12 caracteres"
                ])
                ->setLabelAttributes(['class' => 'control-label']);
        $this->add($resource);

        $this->add(
                [
                    'name' => 'submit',
                    'type' => Element\Submit::class,
                    'attributes' => [
                        'value' => 'Salvar',
                        'id' => 'submitbutton',
                    ]
                ]
        );
    }

}
