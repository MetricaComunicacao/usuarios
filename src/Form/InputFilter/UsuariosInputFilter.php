<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 28/11/2017
 * Time: 15:55
 */

namespace Usuarios\Form\InputFilter;


use Zend\InputFilter\InputFilter;
use Zend\Validator\Db\NoRecordExists;
use Zend\Validator\EmailAddress;
use Zend\Validator\Identical;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class UsuariosInputFilter extends InputFilter
{

    public function __construct($dbAdpter)
    {
        $this->add(
            [
                'name'        => 'id',
                'required'    => true,
                'allow_empty' => true
            ]
        );
        $this->add(
            array(
                'name'       => 'username',
                'required'   => true,
                'validators' => array(
                    [
                        'name'    => 'StringLength',
                        'options' => array(
                            'min'      => 3,
                            'max'      => 255,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Requer no mínimo 3 caracteres!'
                            ]
                        ),
                    ],
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'O campo Username é requerido!'
                         ]
                     ]
                    ],
                    new NoRecordExists(
                        [
                            'table'    => 'usuarios',
                            'field'    => 'username',
                            'adapter'  => $dbAdpter,
                            'messages' => [NoRecordExists::ERROR_RECORD_FOUND => "Esse usuário já esta cadastrado no nosso sistema"]
                        ]
                    )
                ),
            )
        );

        $emailValidator = new EmailAddress();
        $emailValidator->setMessage("Email Invalido!");
        $this->add(
            array(
                'name'       => 'email',
                'required'   => true,
                'validators' => array(
                    $emailValidator,
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'O campo Email é requerido!'
                         ]
                     ]
                    ],
                    new NoRecordExists(
                        [
                            'table'    => 'usuarios',
                            'field'    => 'email',
                            'adapter'  => $dbAdpter,
                            'messages' => [NoRecordExists::ERROR_RECORD_FOUND => "Esse email já esta cadastrado no nosso sistema"]
                        ]
                    )
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'displayname',
                'required'   => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'min' => 3,
                            'max' => 128,
                        ),
                    ),
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'O campo Username é requerido!'
                         ]
                     ]
                    ],
                ),
            )
        );
        $this->add(
            array(
                'name'       => 'password',
                'required'   => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'min' => 6,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Requer no mínimo 6 caracteres!'
                            ]
                        ),

                    ),
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'Uma senha é requerida!'
                         ]
                     ]
                    ],
                ),
            )
        );


        $this->add(
            array(
                'name'       => 'passwordVerify',
                'required'   => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'min' => 6,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Requer no mínimo 6 caracteres!'
                            ]
                        ),

                    ),
                    array(
                        'name'    => 'Identical',
                        'options' => array(
                            'token' => 'password',
                            'messages' => [
                                Identical::MISSING_TOKEN => 'As senhas devem ser iguais!',
                                Identical::NOT_SAME => 'As senhas devem ser iguais!'
                            ]
                        ),


                    ),
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'É necessário confirmar a senha!'
                         ]
                     ]
                    ],
                ),
            )
        );
    }


}