<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 28/11/2017
 * Time: 15:55
 */

namespace Usuarios\Form\InputFilter;


use Zend\InputFilter\InputFilter;
use Zend\Validator\Db\NoRecordExists;
use Zend\Validator\EmailAddress;
use Zend\Validator\Identical;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class ResourceInputFilter extends InputFilter
{

    public function __construct($dbAdpter)
    {
        $this->add(
            [
                'name'        => 'id',
                'required'    => true,
                'allow_empty' => true
            ]
        );
        $this->add(
            array(
                'name'       => 'resource',
                'required'   => true,
                'validators' => array(
                    [
                        'name'    => 'StringLength',
                        'options' => array(
                            'min'      => 3,
                            'max'      => 12,
                            'messages' => [
                                StringLength::TOO_SHORT => 'Requer no mínimo 3 caracteres!'
                            ]
                        ),
                    ],
                    ['name'    => NotEmpty::class,
                     'options' => [
                         'messages' => [
                             NotEmpty::IS_EMPTY => 'O campo permissão é requerido!'
                         ]
                     ]
                    ],
                    new NoRecordExists(
                        [
                            'table'    => 'resources',
                            'field'    => 'resource',
                            'adapter'  => $dbAdpter,
                            'messages' => [NoRecordExists::ERROR_RECORD_FOUND => "Essa permissão já esta cadastrado no nosso sistema"]
                        ]
                    )
                ),
            )
        );        
    }


}