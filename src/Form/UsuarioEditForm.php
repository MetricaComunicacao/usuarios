<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 18:06
 */

namespace Usuarios\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class UsuarioEditForm extends Form
{
    public function __construct($name = null, $options = null, $filter = null)
    {
        parent::__construct("form");


        $id = new Element\Hidden("id");
        $this->add($id);

        $username = new Element\Text("username");
        $username->setLabel("Username");
        $username->setAttributes(['class' => 'form-control','disabled'=>'disabled'])
            ->setLabelAttributes(['class' => 'control-label']);
        $this->add($username);

        $email = new Element\Text("email");
        $email->setLabel("Email");
        $email->setAttributes(['class' => 'form-control','disabled'=>'disabled'])
            ->setLabelAttributes(['class' => 'control-label']);

        $this->add($email);

        $this->add(
            [
                'name'       => 'displayname',
                'type'       => Element\Text::class,
                'options'    => [
                    'label' => 'Nome do Usuário'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]
        );
        $role = new Element\Select("role");
        $role->setName("role")
            ->setLabel("Função")
            ->setValueOptions($options["roleOptions"])
            ->setLabelAttributes(['class' => 'control-label'])
            ->setAttributes(['class' => 'form-control']);
        $this->add($role);

        $this->add(
            [
                'name'       => 'submit',
                'type'       => Element\Submit::class,
                'attributes' => [
                    'value' => 'Salvar Usuário',
                    'id'    => 'submitbutton'
                ]
            ]
        );
    }
}