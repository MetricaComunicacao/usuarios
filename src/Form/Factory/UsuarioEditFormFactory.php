<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 18:39
 */

namespace Usuarios\Form\Factory;


use Psr\Container\ContainerInterface;
use Usuarios\Form\UsuarioEditForm;
use Usuarios\Model\Mapper\RoleTable;

class UsuarioEditFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $roleTable = $container->get(RoleTable::class);
        $roleOptions = $roleTable->getOptionsSelect();
        $options = ["roleOptions" => $roleOptions];
        $form = new UsuarioEditForm("form", $options);
        return $form;
    }
}