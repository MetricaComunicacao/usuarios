<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 16:18
 */

namespace Usuarios\Form\Factory;

use Psr\Container\ContainerInterface;
use Usuarios\Form\ResourceForm;
use Zend\Db\Adapter\AdapterInterface;
use Usuarios\Form\InputFilter\ResourceInputFilter;

class ResourceFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        $form = new ResourceForm("form");
        $form->setInputFilter(new ResourceInputFilter($dbAdapter));
        return $form;
    }

}