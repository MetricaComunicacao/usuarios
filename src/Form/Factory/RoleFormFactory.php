<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 16:18
 */

namespace Usuarios\Form\Factory;

use Psr\Container\ContainerInterface;
use Usuarios\Form\RoleForm;
use Usuarios\Model\Entity\Role;
use Usuarios\Model\Mapper\ResourceTable;
use Usuarios\Model\Mapper\RoleTable;

class RoleFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $roleTable = $container->get(RoleTable::class);
        $roleOptions = $roleTable->getOptionsSelect();
        $roleOptions[0] = "Nenhum";
        $resourceTable = $container->get(ResourceTable::class);
        $resourceOptions = $resourceTable->getOptionsCheck();
        $options = ["roleOptions" => $roleOptions];
        $form = new RoleForm("form", $options, $resourceOptions);
        return $form;
    }

}