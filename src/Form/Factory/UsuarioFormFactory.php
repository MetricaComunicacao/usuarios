<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 18:39
 */

namespace Usuarios\Form\Factory;


use Psr\Container\ContainerInterface;
use Usuarios\Form\InputFilter\UsuariosInputFilter;
use Usuarios\Form\UsuarioForm;
use Usuarios\Model\Mapper\RoleTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Validator\Db\NoRecordExists;

class UsuarioFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $roleTable = $container->get(RoleTable::class);
        $roleOptions = $roleTable->getOptionsSelect();
        $dbAdapter = $container->get(AdapterInterface::class);
        $options = ["roleOptions" => $roleOptions];
        $filter = null;
        $form = new UsuarioForm("form", $options, $filter);
        $form->setInputFilter(new UsuariosInputFilter($dbAdapter));
        return $form;
    }
}