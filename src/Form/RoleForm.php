<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 16:17
 */

namespace Usuarios\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class RoleForm extends Form
{
    public function __construct($name = null,$options,$resources)
    {
        parent::__construct($name);

        $id = new Element\Hidden("id");
        $this->add($id);

        $role = new Element\Text("role");
        $role->setLabel("Função");
        $role->setAttributes(['class' => 'form-control'])
            ->setLabelAttributes(['class' => 'control-label']);
        $this->add($role);

        $modulos = new Element\MultiCheckbox("resources");
        $modulos->setLabel("Permissões")
            ->setAttributes(['class' => 'form-control'])
            ->setLabelAttributes(['class' => 'control-label']);
        $modulos->setValueOptions($resources);
        $this->add($modulos);

        $this->add(
            [
                'name'       => 'submit',
                'type'       => Element\Submit::class,
                'attributes' => [
                    'value' => 'Salvar',
                    'id'    => 'submitbutton'
                ]
            ]
        );
    }
}