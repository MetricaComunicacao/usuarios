<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:33
 */

namespace Usuarios\Model\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Usuarios\Model\Entity\Resource;
use Usuarios\Model\Mapper\ResourceTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class ResourceTableFactory implements FactoryInterface
{


    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Resource());
        $tableGateway = new TableGateway('resources', $dbAdapter, null, $resultSetPrototype);
        return new ResourceTable($tableGateway);
    }
}