<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:33
 */

namespace Usuarios\Model\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Usuarios\Model\Entity\Role;
use Usuarios\Model\Mapper\RoleTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class RoleTableFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Role());
        $tableGateway = new TableGateway('roles', $dbAdapter, null, $resultSetPrototype);
        return new RoleTable($tableGateway);
    }
}