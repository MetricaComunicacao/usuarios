<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:33
 */

namespace Usuarios\Model\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Usuarios\Model\Entity\Usuario;
use Usuarios\Model\Mapper\UsuarioTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class UsuarioTableFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $dbAdapter = $container->get(AdapterInterface::class);
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Usuario());
        $tableGateway = new TableGateway('usuarios', $dbAdapter, null, $resultSetPrototype);
        return new UsuarioTable($tableGateway);
    }
}