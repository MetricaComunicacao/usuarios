<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 24/11/2017
 * Time: 16:25
 */

namespace Usuarios\Model\Entity;

use Zend\Hydrator\ClassMethods;

class Usuario
{
    public $id;
    public $username;
    public $email;
    public $displayname;
    public $password;
    public $state;
    public $role;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getDisplayname()
    {
        return $this->displayname;
    }

    /**
     * @param mixed $displayname
     */
    public function setDisplayname($displayname)
    {
        $this->displayname = $displayname;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }


    /**
     * Extract values from an object
     * @return array
     */
    public function toArray()
    {
        $hyd = new ClassMethods();
        $data = $hyd->extract($this);
        return $data;
    }


    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array $data
     *
     */
    public function exchangeArray(array $data)
    {
        $hyd = new ClassMethods();
        $hyd->hydrate($data, $this);
    }

    protected function mapField($keyFrom, $keyTo, array $array)
    {
        $array[$keyTo] = $array[$keyFrom];
        unset($array[$keyFrom]);

        return $array;
    }

}