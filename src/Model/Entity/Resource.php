<?php

namespace Usuarios\Model\Entity;

use Zend\Hydrator\ClassMethods;

class Resource
{
    public $id;
    public $resource;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param mixed $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    public function exchangeArray(array $options)
    {
        $hydrator = new ClassMethods();
        $hydrator->hydrate($options, $this);
    }

    public function toArray()
    {
        $hidrator = new ClassMethods();
        return $hidrator->extract($this);
    }
}