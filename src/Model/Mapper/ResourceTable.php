<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:10
 */

namespace Usuarios\Model\Mapper;

use RuntimeException;

use Usuarios\Model\Entity\Resource;
use Zend\Db\TableGateway\TableGatewayInterface;

class ResourceTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function insert(Resource $data)
    {
        if (!is_object($data)) {
            throw new InvalidArgumentException(
                "Mapeamento tem que ser do tipo objeto"
            );
        }
        $data->setId(0);
        $this->tableGateway->insert($data->toArray());
    }

    public function save(Resource $data)
    {
        $id = $data->getId();

        if (!$this->find($id)) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }
        $this->tableGateway->update($data->toArray(), ['id' => $id]);
    }

    public function getOptionsSelect()
    {
        $rows = $this->tableGateway->select();
        $options = [];
        foreach ($rows as $row) {
            $options[$row->getId()] = $row->getRole();
        }
        return $options;
    }

    public function find($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }
        return $row;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }

    public function getOptionsCheck()
    {
        $rows = $this->tableGateway->select();
        $options = [];
        foreach ($rows as $row) {
            $options[$row->getResource()] = $row->getResource();
        }
        return $options;
    }
}