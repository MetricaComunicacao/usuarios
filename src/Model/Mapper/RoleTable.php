<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:10
 */

namespace Usuarios\Model\Mapper;

use RuntimeException;
use Usuarios\Model\Entity\Role;
use Zend\Db\TableGateway\TableGatewayInterface;

class RoleTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function insert(Role $user)
    {
        if (!is_object($user)) {
            throw new InvalidArgumentException(
                "Mapeamento tem que ser do tipo objeto"
            );
        }
        $user->setId(0);
        $this->tableGateway->insert($user->toArray());
    }

    public function save(Role $user)
    {
        $data = $user->toArray();
        $id = $user->getId();
        if (!$this->find($id)) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }
        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function getOptionsSelect()
    {
        $rows = $this->tableGateway->select();
        $options = [];
        foreach ($rows as $row) {
            $options[$row->getId()] = $row->getRole();
        }
        return $options;
    }
    public function getAcl()
    {
        $rows = $this->tableGateway->select();
        $options = [];
        foreach ($rows as $row) {
            $resources = explode(",",$row->getResources());
            $options[$row->getRole()] = $resources;
        }
        
        return $options;
    }

    public function find($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current(); //$row == Post
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }
        return $row;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }

}