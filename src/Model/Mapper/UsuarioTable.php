<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 11:10
 */

namespace Usuarios\Model\Mapper;

use RuntimeException;
use Usuarios\Model\Entity\Usuario;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Crypt\Password\Bcrypt;

class UsuarioTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getOptionsSelect()
    {
        $rows = $this->tableGateway->select();
        $options = [];
        foreach ($rows as $row) {
            $options[$row->getId()] = $row->getRole();
        }
        return $options;
    }

    public function insert(Usuario $user)
    {
        if (!is_object($user)) {
            throw new InvalidArgumentException(
                "Mapeamento tem que ser do tipo objeto"
            );
        }

        $bcrypt = new Bcrypt();
        $bcrypt->setCost(14);
        $user->setPassword($bcrypt->create($user->getPassword()));
        $user->setId(0);
        $this->tableGateway->insert($user->toArray());
    }

    public function save(Usuario $user)
    {
        $data = $user->toArray();

        $id = $user->getId();

        if (!$this->find($id)) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }
        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function find($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current(); //$row == Post

        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not retrieve the row %d', $id
            ));
        }

        return $row;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }

}