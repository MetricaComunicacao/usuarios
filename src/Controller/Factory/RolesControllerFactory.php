<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 11:35
 */

namespace Usuarios\Controller\Factory;

use Interop\Container\ContainerInterface;
use Usuarios\Controller\RolesController;
use Usuarios\Form\RoleForm;
use Usuarios\Model\Mapper\RoleTable;

class RolesControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $roleTable = $container->get(RoleTable::class);
        $roleForm = $container->get(RoleForm::class);
        return new RolesController($roleTable, $roleForm);
    }

}