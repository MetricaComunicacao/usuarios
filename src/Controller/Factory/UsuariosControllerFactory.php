<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 27/11/2017
 * Time: 16:14
 */

namespace Usuarios\Controller\Factory;


use Interop\Container\ContainerInterface;
use Usuarios\Controller\UsuariosController;
use Usuarios\Form\UsuarioEditForm;
use Usuarios\Form\UsuarioForm;
use Usuarios\Model\Mapper\UsuarioTable;

class UsuariosControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userTable = $container->get(UsuarioTable::class);
        $userForm = $container->get(UsuarioForm::class);
        $editForm = $container->get(UsuarioEditForm::class);
        return new UsuariosController($userTable, $userForm, $editForm);
    }
}