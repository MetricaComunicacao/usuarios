<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 11:35
 */

namespace Usuarios\Controller\Factory;

use Interop\Container\ContainerInterface;
use Usuarios\Controller\ResourcesController;
use Usuarios\Form\ResourceForm;
use Usuarios\Model\Mapper\ResourceTable;

class ResourcesControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $roleTable = $container->get(ResourceTable::class);
        $roleForm = $container->get(ResourceForm::class);
        return new ResourcesController($roleTable, $roleForm);
    }

}