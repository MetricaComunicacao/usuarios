<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Usuarios\Controller;

use Usuarios\Model\Entity\Usuario;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UsuariosController extends AbstractActionController
{
    private $table;
    private $form;
    private $editForm;

    public function __construct($table, $form, $editForm)
    {
        $this->table = $table;
        $this->form = $form;
        $this->editForm = $editForm;
    }

    public function indexAction()
    {
        $postTable = $this->table;

        return new ViewModel(
            [
                'posts' => $postTable->fetchAll()
            ]
        );
    }

    public function addAction()
    {
        $form = $this->form;
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return ['form' => $form];
        }

        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return ['form' => $form];
        }
        $data = $request->getPost();

        $usuario = new Usuario();
        $usuario->exchangeArray($data->toArray());
        $this->table->insert($usuario);

        return new ViewModel(
            [
                "form" => $form
            ]
        );
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('usuarios');
        }

        try {
            $post = $this->table->find($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('usuarios');
        }

        $form = $this->editForm;
        $form->setData($post->toArray());
        $form->get('submit')->setAttribute('value', 'Salvar');

        $request = $this->getRequest();

        if (!$request->isPost()) {
            return [
                'id'   => $id,
                'form' => $form
            ];
        }

        $form->setData($request->getPost());
        if (!$form->isValid()) {
            return [
                'id'   => $id,
                'form' => $form
            ];
        }
        $data = $request->getPost();
        $post->exchangeArray($data->toArray());
        $this->table->save($post);

        return new ViewModel(
            [
                'id'   => $id,
                'form' => $form
            ]
        );
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('usuarios');
        }
        $this->table->delete($id);
        return $this->redirect()->toRoute('usuarios');
    }
}
