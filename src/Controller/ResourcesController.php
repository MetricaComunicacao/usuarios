<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 04/12/2017
 * Time: 11:34
 */

namespace Usuarios\Controller;

use Usuarios\Model\Entity\Resource;
use Usuarios\Model\Entity\Role;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ResourcesController extends AbstractActionController
{
    private $table;
    private $form;

    public function __construct($table, $form)
    {
        $this->table = $table;
        $this->form = $form;
    }

    public function indexAction()
    {
        $table = $this->table;

        return new ViewModel(
            [
                'posts' => $table->fetchAll()
            ]
        );
    }

    public function addAction()
    {
        $form = $this->form;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if (!$form->isValid()) {                
                return ['form' => $form];
            } else {
                $data = $request->getPost();

                $model = new Resource();
                $model->exchangeArray($data->toArray());
                $this->table->insert($model);
                return $this->redirect()->toRoute('resources');
            }

        }

        return new ViewModel(
            [
                "form" => $form
            ]
        );
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('resources');
        }

        try {
            $post = $this->table->find($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('resources');
        }

        $form = $this->form;
        $form->setData($post->toArray());

        $request = $this->getRequest();

        if (!$request->isPost()) {
            return [
                'id'   => $id,
                'form' => $form
            ];
        } else {
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                return [
                    'id'   => $id,
                    'form' => $form
                ];
            } else {
                $data = $request->getPost();
                $post->exchangeArray($data->toArray());
                $this->table->save($post);
                return $this->redirect()->toRoute('resources');
            }
        }
        return new ViewModel(
            [
                'id'   => $id,
                'form' => $form
            ]
        );
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('resources');
        }
        $this->table->delete($id);
        return $this->redirect()->toRoute('resources');
    }
}